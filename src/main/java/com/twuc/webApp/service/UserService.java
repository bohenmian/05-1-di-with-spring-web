package com.twuc.webApp.service;

import com.twuc.webApp.contract.GetUserResponse;
import com.twuc.webApp.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    private UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public Optional<GetUserResponse> getUser(Long id) {
        return repository.findById(id).map(GetUserResponse::new);
    }
}
